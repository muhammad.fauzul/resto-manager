package com.example.restomanager.repository;

import com.example.restomanager.model.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends JpaRepository<Log, Integer> {
    Log findById(int id);
}
