package com.example.restomanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestoManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestoManagerApplication.class, args);
    }

}
